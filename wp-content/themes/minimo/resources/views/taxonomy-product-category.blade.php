@extends('layouts.app')

@section('content')
 @include('layouts.page-header')
 @include('partials.home.category')


  <div class="container product-container">
  	<div class="row">
  		<div class="col-sm-4">
	  		@include('partials.product.product-filter')
	  	</div>
	  	<div class="col-sm-8">
	  		<div class="row">
		  		@while(have_posts()) @php the_post() @endphp
		  			<div class="col-sm-4">
		  				@include('partials.product.product-list')
		  			</div>
			   @endwhile
			</div>
	  	</div>
  	</div>
 </div>
@endsection