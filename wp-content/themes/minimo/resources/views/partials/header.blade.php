@php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$lang_args = array(
	'show_names' => 1,
	'show_flags' => 0, 
);

if(is_front_page()){
	$menu_theme = "light";
}elseif(is_singular("post")){
	$menu_theme = "light";
}else{
	$menu_theme = "dark";
}

@endphp
<header class="">
<nav class="navigation {{ $menu_theme }}">
	<div class="main-nav-container">
		<div class="container">
			<div class="menu-overlay"></div>
			<a class="navbar-brand" href="/"><img src="{{ $header_image }}" alt="Kaigo"></a> 
			<div class="menu animate">
				@if (has_nav_menu('primary_navigation'))
		    		{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
		  		@endif
		  		
		  		
		  	</div>
		  	<!--<div class="lang_menu">
		  		<?php // pll_the_languages($lang_args); ?>
		  	</div>-->
		  	<button class="hamburger hamburger--squeeze" type="button">
			  	<span class="hamburger-box">
			    	<span class="hamburger-inner"></span>
			  	</span>
			</button>
		</div>

	</div>
</nav>
</header>