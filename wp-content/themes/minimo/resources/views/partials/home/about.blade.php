<?php
$heading = rwmb_meta('about_heading'); 
$heading_slug = rwmb_meta('about_slug'); 
$text = rwmb_meta('about_text'); 
$url = rwmb_meta('about_url'); 

$about_image = rwmb_meta( 'about_image', array( 'size' => 'large' ) );
$image = (isset($about_image)) ? array_values($about_image) : false; 

?>
<div class="container home-section">
	
	<div class="content about">
		<div class="row">
			<div class="col-sm-6">
				<div class="about__image animate animate__fade" <?php if($image) : ?> style="background-image: url({{ $image[0]['url'] }})" <?php endif; ?>>
					<div class="overlay"></div>
				</div>
			</div>
			<div class="col-sm-6">

				<div class="about__content">
					<div class="home-heading-content animate animate__fade">
						<div class="home-heading-content__heading">
							<p class="home-heading-content__slug">{{ $heading_slug }}</p>
							<h2 class="home-heading-content__title">{{ $heading }}</h2>
						</div>
					</div>
					<p class="about__text animate animate__fade">{{ $text }}</p>
					<a class="button--read-more animate animate__fade" href="{{ $url }}">READ MORE</a>
				</div>

			</div>
		</div>
	</div>
</div>