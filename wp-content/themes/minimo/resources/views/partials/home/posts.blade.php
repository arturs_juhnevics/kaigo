<?php
$heading = rwmb_meta('post_heading'); 
$heading_slug = rwmb_meta('post_slug'); 
$url = rwmb_meta('post_url'); 
$query = new WP_Query( array(
    'post_type' => 'post',
    'numberposts' => 3,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_post',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
?>
<div class="container home-section">
	<div class="home-heading-content">
		<div class="home-heading-content__heading animate animate__fade">
			<p class="home-heading-content__slug">{{ $heading_slug }}</p>
			<h2 class="home-heading-content__title">{{ $heading }}</h2>
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more" href="{{ $url }}">VIEW ALL STORIES</a>
		</div>
	</div>
	<div class="posts">
		<div class="row">
			@while($query->have_posts()) @php $query->the_post() @endphp
				<div class="col-sm-4">
			  		@include('partials.content-post')
			  	</div>
		   @endwhile
		</div>
	</div>
	<div class="button-container mob-only">
		<a href="{{ $url }}" class="button">VIEW ALL STORIES</a> 
	</div>
</div>