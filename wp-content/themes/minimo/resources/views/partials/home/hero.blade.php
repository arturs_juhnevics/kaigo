@php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );

@endphp
<div class="hero-wrapper">
	<div class="hero rellax">
	@foreach ($slides as $slide)
		@php
		$image = get_the_post_thumbnail_url($slide->ID);

		$title = get_post_meta($slide->ID, 'slider_title');
		$text = get_post_meta($slide->ID, 'slider_text');
		$buttonText = get_post_meta($slide->ID, 'button_text');
		$buttonUrl = get_post_meta($slide->ID, 'button_url');
		$full_width = get_post_meta($slide->ID, 'full_width_bg');
		$full = '';
		if ( $full_width[0] == '1' ) {
			$full = 'full-width';
		}

		@endphp
		<div class="hero__item animate animate__fade {{ $full }}" style="background-image: url({{ $image }})">
			<div class="overlay"></div>
			<div class="container hero__content">
	            <div class="hero__item__inner rellax-content">
	            	<h2 class="hero__title animate animate__fade">{{ $title[0] }}</h2>
	            		@if($text)
	                    	<div class="hero__text animate animate__fade">{{ $text[0] }}</div>
	                    @endif
	                    @if($buttonUrl)
						<a href="{{ $buttonUrl[0] }}" class="button">{{ $buttonText[0] }}</a> 
						@endif
	           	</div>
	        </div>
		</div>
	@endforeach
	</div>
</div>
