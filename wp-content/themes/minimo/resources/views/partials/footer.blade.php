<?php 
$lang = pll_current_language('slug'); 
$address = rwmb_meta( 'footer_text_'.$lang, array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$phone = rwmb_meta( 'footer_phone_'.$lang, array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$email = rwmb_meta( 'footer_email_'.$lang, array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>
<footer>
	<div class="container">
		<div class="footer-content clearfix animate animate__fade">
			<div class="footer-content__item ">
				<p class="footer-content__item__title">Address</p>
				<p class="footer-content__item__text">{{ $address }}</p>
			</div>
			<div class="footer-content__item">
				<p class="footer-content__item__title">Contact</p>
				<p class="footer-content__item__text"><a href="tel:{{ $phone }}">{{ $phone }}</a></p>
				<p class="footer-content__item__text"><a href="mailto:{{ $email }}">{{ $email }}</a></p>
			</div>
			
			<div class="footer-content__item">
				<p class="footer-content__item__title">Follow</p>
				<p class="footer-content__item__text"><a href="">facebook</a></p>
				<p class="footer-content__item__text"><a href="">instagram</a></p>
			</div>
			<div class="footer-content__item">
				<p class="footer-content__item__title">Pages</p>
				@if (has_nav_menu('primary_navigation'))
		    		{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
		  		@endif
			</div>
		</div>
	</div>
</footer>

