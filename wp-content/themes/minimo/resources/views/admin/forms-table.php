<div class="wrap">

	<style>
		.unread a{ color: #fab000; }
	</style>
	
	<form method="POST">
		<?php $types = $this->get( false, 'type' ); if( isset( $_GET['forms-filter'] ) ) $filter = $_GET['forms-filter']; else $filter = ''; ?>
		<ul class="subsubsub">
			<li class="all">
				<a href="<?php echo admin_url( 'admin.php?page=df-forms' ); ?>" <?php if( !strlen($filter) ) echo 'class="current"'; ?>>
					All
				</a>
				 |
			</li>
			<?php foreach ($types as $type) { ?>
				<li>
					<a <?php if( $filter == $type->type ) echo 'class="current"'; ?>
						href="<?php echo admin_url( 'admin.php?page=df-forms&forms-filter=' . urlencode( $type->type ) ); ?>">
						<?php echo $type->type; ?>
					</a> |
				</li>
			<?php } ?>
		</ul>
		<div class="tablenav top">
			<?php echo '<input type="submit" class="submitdelete deletion" value="Delete selected" />'; ?>
		</div>
		<table class="wp-list-table widefat fixed">
			
			<thead>
				<tr>
					<th class="check-column">
						<label class="screen-reader-text" for="cb-select-all-1">Select All</label>
						<input id="cb-select-all-1" type="checkbox">
					</th>
					<th scope="col" id="title" class="manage-column column-title" style="">
						Type
					</th>
					<th>
						Email
					</th>
					<th class="column-date">
						Date received
					</th>
				</tr>
			</thead>

			<tbody id="the-list">
				<?php if( ! empty( $rows ) ) : ?>
				<?php $i = 0; foreach ($rows as $key => $row) {
					$values = unserialize($row->value);
					echo '<tr class="';
					if( $i % 2 == 0 ){
						echo ' alternate';
					}
					if( $row->status == 'new' ){
						echo ' unread';
					}
					echo '">';
					// checkbox
					echo '<th scope="row" class="check-column">';
					echo '<label class="screen-reader-text" for="cb-select-'.$row->id.'">'.$row->type.'</label>';
					echo '<input id="cb-select-'.$row->id.'" type="checkbox" name="post[]" value="'.$row->id.'">';
					echo '</th>';

					// type
					echo '<td><strong><a href="'.admin_url('admin.php?page=df-forms&form-id='.$row->id).'">' . $row->type . '</a></strong>';
					echo '<div class="row-actions">';
					echo '<span class="edit"><span class="trash">';
					echo '<a class="submitdelete" title="Delete this item" href="'.admin_url('admin.php?page=df-forms&form-delete='.$row->id).'">Trash</a> | </span>';
					echo '<span class="view"><a href="'.admin_url('admin.php?page=df-forms&form-id='.$row->id).'" rel="permalink">View</a></span></span>';
					echo '</div></td>';

					// name and lastname
					echo '<td>' . json_decode($values['email']) . '</td>';

					// date
					echo '<td>' . date('d.m.Y', strtotime( $row->date_created )) . '</td>';
					echo '</tr>';
					$i++;
				} ?>
			<?php else : ?>
				<tr>
					<td></td>
					<td>No results to show</td>
					<td></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</form>
</div>