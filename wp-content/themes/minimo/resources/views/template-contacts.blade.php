{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header-simple')
  @while(have_posts()) @php the_post() @endphp

<div class="contact-section2">
  <div class="contact-section2-content">
    <div class="contact-section2-content__text"></div>
    <div class="map animate animate__fade-up">
        <?php
        $map = rwmb_get_value('contact_map');
        $pins = rwmb_meta('contact_map_pin', array( 'limit' => 1 ));
        $pin = reset( $pins );

        ?>
        <?php if (!empty($map)) : ?>
            <div class="contacts__map">
              <div id="map-wrap"
                     class="contacts__map__wrap"
                       data-lng="<?php echo $map['longitude'] ?>"
                     data-lat="<?php echo $map['latitude'] ?>"
                     data-zoom="<?php echo $map['zoom']; ?>"
                     data-pin="<?php echo $pin['url']; ?>">
                       
              </div>
            </div>
        <?php endif; ?>
    </div>

<div class="container contacts">
  
  <div class="contacts__general animate animate__fade">
    <div class="row"> 

      <div class="col-sm-6">
        <div class="main-contacts">
          <?php 
            $general = rwmb_meta('general');
          ?>

          <a class="info-icon phone" href="tel:{{ $general['contacts_phone'] }}"><span class="info-icon__icon"><i class="fas fa-phone-square-alt"></i></span>{{ $general['contacts_phone'] }}</a>
          <a class="info-icon email" href="mailto:{{ $general['contacts_email'] }}"><span class="info-icon__icon"><i class="fas fa-envelope-square"></i></span>{{ $general['contacts_email'] }}</a>
         <p class="info-icon address"><span class="info-icon__icon"><i class="fas fa-map-marker-alt"></i></span>{{ $general['contacts_address'] }}</p>
        </div>
        
      
        <div class="contacts__persons">
          <div class="row">
            <?php 
              $team = rwmb_meta('team');
              ?>
              <?php foreach ($team as $value) { ?>
              
                <div class="col-sm-6 team animate animate__fade">
                  
                  <p class="team__pos"><?php echo $value['position']; ?></p>
                  <p class="team__name"><?php echo $value['name']; ?></p>
                  <a href="tel:<?php echo $value['phone']; ?>" class="info-icon phone"><span class="info-icon__icon"><i class="fas fa-phone-square-alt"></i></span><?php echo $value['phone']; ?></a>
                  <a href="mailto:<?php echo $value['email']; ?>" class="info-icon email"><span class="info-icon__icon"><i class="fas fa-envelope-square"></i></span><?php echo $value['email']; ?></a>
                </div>
              <?php } ?>
          </div>
        </div>

      </div>

      <div class="col-sm-6">
        
      </div>

    </div>
    
  </div>

  
</div>
</div>


  @endwhile
  
@endsection


   			