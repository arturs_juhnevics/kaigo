@extends('layouts.app')

@section('content')
@include('layouts.page-header-simple')
<?php
$description = rwmb_meta( 'p_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
  <div class="archive-desc animate animate__fade">
    <p>{{ $description }}</p>
  </div>
</div>

<div class="container posts">
  <div class="row">
    @while (have_posts()) @php the_post() @endphp
      <div class="col-sm-4">
        @include('partials.content-'.get_post_type())
      </div>
    @endwhile
  </div>
</div>
  

  {!! get_the_posts_navigation() !!}
@endsection
