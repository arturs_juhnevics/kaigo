{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')

<?php 
$enable_posts = rwmb_meta('enable_posts'); 
?>

  @while(have_posts()) @php the_post() @endphp
    @include('partials.home.hero')
    
    @include('partials.home.work')
    @include('partials.home.partners')
    @include('partials.home.about')
    <?php if($enable_posts) : ?>
    	@include('partials.home.posts')
    <?php endif; ?>

  @endwhile
@endsection