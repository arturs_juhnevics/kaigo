var home = {
	init: function (){
		var rellax = new Rellax('.rellax', {
			speed: -6,
		});
		var rellax_content = new Rellax('.rellax-content', {
			speed: 6,
		});
	}
}
home.init();