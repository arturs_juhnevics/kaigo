<?php
namespace Roots\Sage\Admin;

function move_excerpt_meta_box($post)
{
    return;
    if (post_type_supports($post->post_type, 'excerpt') && !$post->is_front_a) {
        remove_meta_box('postexcerpt', $post->post_type, 'normal'); ?>
        <h2 style="padding: 20px 0 0;"><?php echo __('Short description (Excerpt)') ?></h2>
        <?php post_excerpt_meta_box($post);
    }
}

add_action('edit_form_after_title', __NAMESPACE__ . '\\move_excerpt_meta_box');
/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function wpcodex_add_excerpt_support_for_pages()
{
    add_post_type_support('page', 'excerpt');
}

add_action('init', __NAMESPACE__ . '\\wpcodex_add_excerpt_support_for_pages');
/**
 * Additional tinymce element
 */
function cc_add_tinymce()
{
    add_filter('mce_external_plugins', __NAMESPACE__ . '\\cc_add_tinymce_plugin');
    add_filter('mce_buttons', __NAMESPACE__ . '\\cc_add_tinymce_button');
}

add_action('admin_head', __NAMESPACE__ . '\\cc_add_tinymce');

function cc_add_tinymce_plugin($plugin_array)
{
    //$plugin_array['buttons'] = get_template_directory_uri().'/assets/scripts/tinymce/buttons.js';
    //$plugin_array['cab_gallery'] = get_template_directory_uri().'/assets/scripts/gallery.js';

    return $plugin_array;
}

function cc_add_tinymce_button($buttons)
{
    array_push($buttons, 'Buttons');
    //array_push($buttons, 'Gallery');

    return $buttons;
}

function my_set_gallery_default_five_columns( $settings ) {
    $settings['galleryDefaults']['columns'] = '4';
    $settings['galleryDefaults']['link'] = 'file';
    $settings['galleryDefaults']['size'] = 'large';

    return $settings;
}
add_filter( 'media_view_settings', __NAMESPACE__ . '\\my_set_gallery_default_five_columns', 10 );

/**
 * Global admin panel css changes
 */
function caballero_custom_styles()
{
    echo '<style>
    .metabox-holder .postbox>h3, .metabox-holder .stuffbox>h3, .metabox-holder h2.hndle, .metabox-holder h3.hndle {
      background-color: #3f515c;
      color: #FFF;
    }
    .vp-metabox .vp-field .buttons{
        margin-top: 0;
    }
  </style>';
}

add_action('admin_head', __NAMESPACE__ . '\\caballero_custom_styles');