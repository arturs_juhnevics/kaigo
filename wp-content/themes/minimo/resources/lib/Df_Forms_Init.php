<?php

class Df_Forms_Init{

	public $wpdb;
	public $table_name;
	public $fields;
	//public $locales;

	public function __construct( $name ){

		global $wpdb;
		global $polylang;

		$this->wpdb = $wpdb;
		$this->table_name = $name;
		$this->set_table();
		$this->set_fields();

		// get available languages
		//$this->locales = $polylang->model->get_languages_list();
		
		if( isset($_GET['form-delete']) ){
			$this->delete( $_GET['form-delete'], true );

			wp_redirect( admin_url('admin.php?page=df-forms') );
		}

		if( isset( $_POST['post'] ) ){

			$posts = $_POST['post'];
			$posts = urlencode( implode(',', $posts) );

			wp_redirect('admin.php?page=df-forms&form-delete='. $posts . '&delete-multiple=true');
		}
	}

	public function add_menu_page(){

		add_menu_page( 'Forms',
			           'Forms',
			           'edit_posts',
			           'df-forms',
			           array( $this, 'render_index' ),
			           null,
			           35 );
	}

	public function render_index(){

		if( isset( $_GET['form-id'] ) ){

			// show single form
			$data = $this->get( $id = $_GET['form-id'] );
			if( $data->status == 'new' ){
				$this->mark_as_read( $id );
			}
			include locate_template( '/views/admin/form-single.php' );
		}elseif( isset( $_GET['forms-filter'] ) ){

			$where = urldecode( $_GET['forms-filter'] );
			$rows = $this->get( false, false, array('type', $where) );
			echo '<h3>Forms</h3>';
			include locate_template( '/views/admin/forms-table.php' );
		}else{

			// show all forms
			$rows = $this->get();
			echo '<h3>Forms</h3>';
			include locate_template( '/views/admin/forms-table.php' );
		}

	}


	public function set_fields(){

		$this->fields = array(
			array(
				'title' => 'Klienta karte',
				array(
					'title' => 'Headline',
					'name'  => 'client-success-headline',
					'type'  => 'text'
					),
				array(
					'title' => 'Text',
					'name'  => 'client-success-text',
					'type'  => 'textarea'
					),
				array(
					'title' => 'Back text',
					'name'  => 'client-success-link',
					'type' => 'text'
					),
				),
			array(
				'title' => 'Vīna vakars draugiem',
				array(
					'title' => 'Headline',
					'name'  => 'evening-success-headline',
					'type'  => 'text'
					),
				array(
					'title' => 'Text',
					'name'  => 'evening-success-text',
					'type'  => 'textarea'
					)
				),
			);
	}



	public function render_forms_settings(){

		if( isset( $_POST['df-success-settings'] ) ){

			foreach ($_POST as $key => $value) {

				if( $key == 'df-success-settings' ){
					continue;
				}

				if( ! strlen( $value ) ){
					if( get_option( $key ) ){
						delete_option( $key );
					}
					continue;
				}

				if( get_option( $key ) ){
					update_option( $key, $value );
				}else{
					add_option( $key, $value );
				}
			}
		}

		include locate_template( 'templates/admin/forms-settings.php' );
	}



	public function get( $id = false, $groupby = false, $where = false ){

		if( $id !== false ){
			$sql = "SELECT * FROM $this->table_name WHERE id = $id";
		}else{
			if( $groupby !== false ){
				$sql = "SELECT * FROM $this->table_name GROUP BY $groupby";
			}elseif( $where !== false ){
				$sql = "SELECT * FROM $this->table_name WHERE $where[0] = '$where[1]'";
			}else{
				$sql = "SELECT * FROM $this->table_name";
			}
		}

		$sql .= ' ORDER BY date_created DESC;';

		$rows = $this->wpdb->get_results( $sql, 'OBJECT' );
		if( $id !== false ){
			$rows = $rows[0];
		}
		return $rows;
	}

	private function delete( $id = false, $multiple = false ){

		if( $id === false ){
			return false;
		}

		if( $multiple === false ){
			$where = array(
				'id' => $id
				);
		}else{

			//$ids = unserialize( $id );
			$ids = explode( ',', urldecode( $id ) );
			$where = array();

			foreach ($ids as $id) {
				$where['id'] = $id;
				$this->wpdb->delete( $this->table_name, $where );
			}
		}
	}

	private function mark_as_read( $id ){

		if( (isset($where) && $where === false) || (isset($what) && $what === false) ){
			return false;
		}

		$sql = "UPDATE $this->table_name SET status = 'open' WHERE id = $id";

		$this->wpdb->query( $sql );
	}

	public function set_table(){

		$sql = "CREATE TABLE $this->table_name (
			id int(9) NOT NULL AUTO_INCREMENT,
			type varchar(255),
			value longtext,
			date_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			status varchar(255) DEFAULT 'new',
			UNIQUE KEY id (id)
		);";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}