<?php
function widgets_area_init() {

	register_sidebar( array(
		'name'          => 'Product filter',
		'id'            => 'product_filter',
		'before_widget' => '<div class="product-filter">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="product-filter__title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'widgets_area_init' );
