    <?php

class Product{

    public function __construct() {

        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'template-product.php'
        ));
        $page = reset($pages);
        $pageName = isset($page->post_name) ? $page->post_name : 'product';


        $labels = array(
            'name'               => "Products",
            'singular_name'      => "Products",
            'menu_name'          => "Products",
            'add_new_item'       => "Add product",
            'new_item'           => "New product",
            'edit_item'          => "Edit product",
            'view_item'          => "View product",
            'all_items'          => "All product",
            'search_items'       => "Search Products",
            'not_found'          => "No Product found",
            'not_found_in_trash' => "No Product found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),    
        );

        register_post_type('product', $args);

        add_filter( 'rwmb_meta_boxes', 'product_single_meta_boxes' );

        function product_single_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'product',
                'title'      => 'Product',
                'post_types' => 'product',  
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'        => 'feat_product',
                        'name'      => 'Featured product',
                        'type'      => 'switch',
                        
                        // Style: rounded (default) or square
                        'style'     => 'rounded',

                        // On label: can be any HTML
                        'on_label'  => 'Yes',

                        // Off label
                        'off_label' => 'No',
                    ),
                  
                    array(
                        'id'               => 'product_gallery',
                        'name'             => 'Gallery',
                        'type'             => 'image_advanced',
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ),     
                    array(
                        'id'               => 'icons',
                        'name'             => 'Product features',
                        'type'             => 'image_advanced',
                        'max_status'       => 'false',
                        'image_size'       => 'thumbnail',
                    ), 
                    array(
                        'id'               => 'short_description',
                        'name'             => 'Short description',
                        'type'             => 'wysiwyg',
                    ),       
                    
                )
            );

            return $meta_boxes;
        }

    }

}
new Product();