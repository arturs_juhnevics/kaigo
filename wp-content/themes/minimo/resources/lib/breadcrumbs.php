<?php
// CUSTOM BREADCRUMBS
// Breadcrumbs
function breadcrumbs() {
     $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
    echo '<li class="breadcrumbs__item"><a href="'.home_url().'" rel="nofollow">Sākums</a></li>';
    if (is_category() || is_single()) {
        echo "<li class='breadcrumbs__item'>&nbsp;&nbsp;&#187;&nbsp;&nbsp;</li>";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "<li class='breadcrumbs__item'>&nbsp;&nbsp;&#187;&nbsp;&nbsp;</li>";
        echo the_title();
    } elseif (is_search()) {
        echo "<li class='breadcrumbs__item'>&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... </li>";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
    echo '</ul>';
}
function get_breadcrumbs() {

    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = pll__('Sākums');
    $home_url           = get_the_permalink( get_option('page_on_front') );

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="'.$breadcrums_class.'__item '.$breadcrums_class.'__item--home"><a href="'. $home_url .'">'. $home_title  .'</a></li>';
        echo '<li class="'.$breadcrums_class.'__separator">' . $separator . '</li>';

        $urlString = trim(trim( strtok($_SERVER["REQUEST_URI"],'?'), '/' ), pll_current_language() . '/');
        $pageStrPos = strpos($urlString, '/page/');
        $urlArray = explode( '/', $urlString );
        if($pageStrPos !== false){
            $urlArray = array_slice($urlArray, 0, -2);
        }
        $urlArray = array_slice($urlArray, 0, -1);
        $urlProgress = get_home_url();
        foreach ($urlArray as $urlPart) {
            $urlProgress .= '/' . $urlPart;
            $term = get_term_by( 'slug', $urlPart, 'solution');
            $term = $term ? $term : get_term_by( 'slug', $urlPart, 'product-category');
            if($term && $term->taxonomy == 'categories' && is_single()){
                $pages = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'template-news.php'
                ));
                $postsPage = $pages[0];
                echo '<li class="'.$breadcrums_class.'__item"><a href="'. get_permalink($postsPage->ID) .'">'. $postsPage->post_title .'</a></li>';
                echo '<li class="'.$breadcrums_class.'__separator">' . $separator . '</li>';
            }
            $currentPost = $term ? $term : get_post(url_to_postid($urlProgress . '/'));
            $title = $term ? $currentPost->name : $currentPost->post_title;
            $currentUrl = $term ? get_term_link($term) : $urlProgress . '/';
            echo '<li class="'.$breadcrums_class.'__item"><a href="'. $currentUrl .'">'. $title .'</a></li>';
            echo '<li class="'.$breadcrums_class.'__separator">' . $separator . '</li>';
        }

        echo '</ul>';

    }
}