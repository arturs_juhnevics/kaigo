<?php

$strings = array(
   
    'Fresh articles' => 'General',
    'Share this article' => 'General',
    'All' => 'General',

    'View blog' => 'Buttons',
    'Read more' => 'Buttons',
    'Look more' => 'Buttons',
    'Read less' => 'Buttons',

    'Contact form' => 'Contact-form',
    'Your name' => 'Contact-form',
    'Your email address' => 'Contact-form',
    'Name of the company' => 'Contact-form',
    'Telephone' => 'Contact-form',
    'Subject of text' => 'Contact-form',
    'Send' => 'Contact-form',

    'Are you intrigued and want to become part of our team? Check out vacancies below or send us a message!' => 'Job-page',
    'We are currently looking for the following people:' => 'Job-page',
    'Publication date:' => 'Job-page',
    'Job title:' => 'Job-page',
    'Work load:' => 'Job-page',
    'Experience:' => 'Job-page',
    'Type of working time:' => 'Job-page',

    'All cities' => 'Stores',
    'Plan route' => 'Stores',
    'Close' => 'Stores',
    'City:' => 'Stores',
    'Look more', 'Buttons',

    'Sorry, but the page you were trying to view does not exist.' => '404',
    'You might be also interested in' => 'General',

    'Subscribe to newsletter' => 'Newsletter',
    'Newsletter text' => 'Newsletter',
    'Subscribe' => 'Newsletter',
    'Not now' => 'Newsletter',
    "You have been added to subscriber list" => "Newsletter",
    "You have already subscribed" => "Newsletter",

    'You might be also interested in' => 'Post',

    'R1' => 'Newsletter',
    'R3' => 'Newsletter',


);

if( function_exists('pll_register_string') ) {
    foreach ( $strings as $string => $category ) {
        pll_register_string($string, $string, $category);
    }
}
