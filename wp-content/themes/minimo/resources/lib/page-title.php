<?php
/**
 * Page titles
 */
function page_title()
{
    if (is_home()) {
        if (get_option('page_for_posts', true)) {
            return get_the_title(get_option('page_for_posts', true));
        } else {
            return pll__('Latest Posts');
        }
    } elseif (is_singular('post')) {
        return the_title();

    } elseif (is_archive()) {
        if (is_category()) {

            $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            $type = get_post_type();
            $category = get_the_category();

            return $category[0]->name;
        } elseif (is_post_type_archive()) {
            return post_type_archive_title('', false);

        } else {
            if(!empty(get_query_var('product-category'))){
                $category = get_term_by('slug', get_query_var('product-category'), 'product-category');
                if(!empty(get_option('taxonomy_'. $category->term_id)['term_tags'])){
                    return get_term_by('slug', get_query_var('product-category'), 'product-category')->name.', '.get_option('taxonomy_'. $category->term_id)['term_tags'];
                }
                return get_term_by('slug', get_query_var('product-category'), 'product-category')->name;
            }
            return get_the_archive_title();
        }

    } elseif (is_search()) {
        return sprintf(pll__('Search Results for').' %s', get_search_query());
    } elseif (is_404()) {
        return pll__('Not Found');
    } else {
        return get_the_title();
    }
}