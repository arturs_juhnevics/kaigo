<?php

class Jobs{

    public function __construct() {

        $labels = array(
            'name'               => "Job offers",
            'singular_name'      => "Job offer",
            'menu_name'          => "Job offers",
            'add_new_item'       => "Add job offer",
            'new_item'           => "New job offer",
            'edit_item'          => "Edit job offer",
            'view_item'          => "View job offer",
            'all_items'          => "All job offers",
            'search_items'       => "Search job offers",
            'not_found'          => "No job offer found",
            'not_found_in_trash' => "No job offer found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),  
            
           
        );

        register_post_type('jobs', $args);

        add_filter( 'rwmb_meta_boxes', 'solution_meta_boxes' );
        function solution_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'jobs',
                'title'      => 'Extra',
                'post_types' => array('jobs'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'    => 'pub_date',
                        'name' => 'Publication date',
                        'type'  => 'text',
                    ),
                    array(
                        'id'    => 'job_title',
                        'name' => 'Job title',
                        'type'  => 'text',
                    ),
                    array(
                        'id'    => 'work_load',
                        'name' => 'Work load',
                        'type'  => 'text',
                    ),
                    array(
                        'id'    => 'experiance',
                        'name' => 'Experiance',
                        'type'  => 'text',
                    ),
                    array(
                        'id'    => 'work_time',
                        'name' => 'Type of working time',
                        'type'  => 'text',
                    ),
                        
                )
            );

            return $meta_boxes;
        }


    }

}
new Jobs();