<?php

class Photoshoots{

    public function __construct() {

        $labels = array(
            'name'               => "Photoshoots",
            'singular_name'      => "Photoshoot",
            'menu_name'          => "Photoshoots",
            'add_new_item'       => "Add photoshoot",
            'new_item'           => "New photoshoot",
            'edit_item'          => "Edit photoshoot",
            'view_item'          => "View photoshoot",
            'all_items'          => "All photoshoots",
            'search_items'       => "Search photoshoots",
            'not_found'          => "No photoshoot found",
            'not_found_in_trash' => "No photoshoot found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => false,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title'),  
            
           
        );

        register_post_type('photoshoots', $args);

        add_filter( 'rwmb_meta_boxes', 'photoshoot_meta_boxes' );
        function photoshoot_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'photoshoots',  
                'title'      => 'Images',
                'post_types' => array('photoshoots'),
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'               => 'ps_image',
                        'name'             => 'Images',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 2,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'true',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                        
                )
            );

            return $meta_boxes;
        }


    }

}
new Photoshoots();