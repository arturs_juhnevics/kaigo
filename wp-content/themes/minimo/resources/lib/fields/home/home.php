<?php
add_filter( 'rwmb_meta_boxes', 'home_meta_boxes' );
function home_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'Products',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'product_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
             array(
                'id'   => 'product_slug',
                'name' => 'Slug',
                'type' => 'text',
            ),
            array(
                'id'   => 'product_url',
                'name' => 'URL',
                'type' => 'text',
            ),
        ),
    );
    $meta_boxes[] = array(
        'title'      => 'Partners',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'partners_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
            array(
                'id'   => 'partners_slug',
                'name' => 'Slug',
                'type' => 'text',
            ),
            array(
                'id'   => 'partners_text',
                'name' => 'Text',
                'type' => 'textarea',
            ),
            array(
                'id' => 'partners',
                'clone' => true,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => "Partners",
                'fields' => array(
                    array(
                        'id'   => 'partners_name',
                        'name' => 'Name',
                        'type' => 'text',
                    ),
                    array(
                        'id'   => 'partners_url',
                        'name' => 'URL',
                        'type' => 'text',
                    ),
                    array(
                        'id'               => 'partners_icon',
                        'name'             => 'Icon',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                )
            ),
        ),
    );
     $meta_boxes[] = array(
        'title'      => 'About us',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'about_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
             array(
                'id'   => 'about_slug',
                'name' => 'Slug',
                'type' => 'text',
            ),
             array(
                'id'   => 'about_url',
                'name' => 'URL',
                'type' => 'text',
            ),
            array(
                        'id'               => 'about_image',
                        'name'             => 'Image',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
            array(
                'id'   => 'about_text',
                'name' => 'Text',
                'type' => 'textarea',
            ),
        ),
        
    );
    $meta_boxes[] = array(
        'title'      => 'Posts',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'post_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
             array(
                'id'   => 'post_slug',
                'name' => 'Slug',
                'type' => 'text',
            ),
             array(
                'id'   => 'post_url',
                'name' => 'URL',
                'type' => 'text',
            ),
             array(
                'id'        => 'enable_posts',
                'name'      => 'Enable Posts',
                'type'      => 'switch',
                
                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
        ),
        
    );
    return $meta_boxes;
}