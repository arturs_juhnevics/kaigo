<?php
add_filter( 'rwmb_meta_boxes', 'contacts_meta_boxes' );
function contacts_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'General',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
             array(
                'id' => 'general',
                'clone' => false,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => "General info",
                'fields' => array(
                    array(
                        'id'   => 'contacts_address',
                        'name' => 'Address',
                        'type' => 'text',
                    ),            
                    array(
                        'id'   => 'contacts_email',
                        'name' => 'Email',
                        'type' => 'text',
                    ),
                    array(
                        'id'   => 'contacts_phone',
                        'name' => 'Phone',
                        'type' => 'text',
                    ),
                )
            ),
           array(
                'id' => 'team',
                'clone' => true,
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => array( 'field' => 'name' ),
                'fields' => array(

                    array(
                        'id'   => 'position',
                        'name' => 'Position',
                        'type' => 'text',
                    ),
                    array(
                        'id'   => 'name',
                        'name' => 'Name',
                        'type' => 'text',
                    ),
                    array(
                        'id'   => 'phone',
                        'name' => 'Phone',
                        'type' => 'text',
                    ),
                    array(
                        'id'   => 'email',
                        'name' => 'Email',
                        'type' => 'text',
                    ),
                ),
            ),
        ),

    );
    $meta_boxes[] = array(
            'title'      => 'Map',
            'post_types' => array('page'),
            'include' => array(
                'relation'        => 'OR',
                'template'        => array('views/template-contacts.blade.php')
            ),
            'name' =>'map',
            'context'    => 'normal',
            'priority'   => 'high',
            'fields' => array(

                array(
                    'id'   => 'address',
                    'name' => 'Address',
                    'type' => 'text',
                    'placeholder' => 'Address',
                    'size' => '80',
                ),
                array(
                    'id'            => 'contact_map',
                    'name'          => 'Location',
                    'type'          => 'map',
                    'address_field' => 'address',
                    'api_key'       => 'AIzaSyBaob1XUk6NQ7RmmsB4QuhqjGh3foYE3EE',
                ),
                array(
                    'name' => 'Pin',
                    'id'   => 'contact_map_pin',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                )
            )
        );
    return $meta_boxes;
}