<?php


add_filter( 'mb_settings_pages', 'options_page' );
function options_page( $settings_pages ) {
    $settings_pages[] = array(
        'id'          => 'settings',
        'option_name' => 'settings',
        'menu_title'  => 'Theme Options',
        'columns'     => 1,
        'style'       => 'no-boxes',
        'tabs'        => array(
            'basic' => 'Basic',
            'footer'  => 'Footer',
            '404_page'     => '404 page',
            'products' => 'Products',
            'stories' => 'Stories',
            'social_media' => 'Social Media',
            'custom_scripts' => 'Custom scripts',
            'robots' => 'Robots.txt',
            'def_soc_image' => 'Default social image',
            'redirects' => 'Redirects',
            'gmaps' => 'Google analytics & map',
        ),

    );
    return $settings_pages;
}


add_filter( 'rwmb_meta_boxes', 'options_meta_boxes' );
function options_meta_boxes( $meta_boxes ) {
    if( function_exists( 'pll_the_languages' ) ) {
        global $polylang;
        $languages = $polylang->model->get_languages_list();

        $langTabs = array();
        $footerFields = array();
        $fields404 = array();

        foreach ( $languages as $l ) {

            $langTabs[$l->slug] = array('label' => $l->name);

            array_push($footerFields,
                array(
                    'name' => 'Address',
                    'id'   => 'footer_text_'.$l->slug,
                    'type' => 'text',
                    'tab'  => $l->slug,
                ),
                array(
                    'name' => 'Phone',
                    'id'   => 'footer_phone_'.$l->slug,
                    'type' => 'text',
                    'tab'  => $l->slug,
                ),
                array(
                    'name' => 'Email',
                    'id'   => 'footer_email_'.$l->slug,
                    'type' => 'text',
                    'tab'  => $l->slug,
                )
            );
            array_push($fields404,
                array(
                    'name' => '404 image',
                    'id'   => '404image_'.$l->slug,
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                    'tab'  => $l->slug,
                ),
                array(
                    'name' => '404 page',
                    'id'   => '404text_'.$l->slug,
                    'type' => 'wysiwyg',
                    'raw'     => false,
                    'options' => array(
                        'teeny'         => true,
                        'tinymce' => true,
                        'media_buttons' => false,
                    ),
                    'tab'  => $l->slug,
                )
             );

        }
    }


    $meta_boxes[] = array(
        'id'             => 'basic',
        'title'          => 'basic',
        'settings_pages' => 'settings',
        'tab'            => 'basic',

        'fields' => array(
            array(
                'name' => 'Site Logo',
                'id'   => 'logo',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'name' => 'Favicon',
                'id'   => 'favicon',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'name' => 'Page header',
                'id'   => 'page-header',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'id'        => 'enable_newsletter',
                'name'      => 'Enable newsletter',
                'type'      => 'switch',
                
                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
        ),

        /** Footer **/
        $meta_boxes[] = array(
            'id'             => 'footer',
            'title'          => 'Footer',
            'settings_pages' => 'settings',
            'tab'            => 'footer',
            'tabs'      => $langTabs ,

            // Tab style: 'default', 'box' or 'left'. Optional
            'tab_style' => 'box',

            // Show meta box wrapper around tabs? true (default) or false. Optional
            'tab_wrapper' => true,
            'fields' => $footerFields,
        ),

        $meta_boxes[] = array(
            'id'             => '404page',
            'title'          => '404 page',
            'settings_pages' => 'settings',
            'tab'            => '404_page',
            'tabs'      => $langTabs ,

            // Tab style: 'default', 'box' or 'left'. Optional
            'tab_style' => 'box',

            // Show meta box wrapper around tabs? true (default) or false. Optional
            'tab_wrapper' => true,

            'fields' => $fields404,
        ),

        $meta_boxes[] = array(
            'id'             => 'products',
            'title'          => 'products',
            'settings_pages' => 'settings',
            'tab'            => 'products',

            'fields' => array(
                array(
                    'name' => 'Product archive text',
                    'id'   => 'p_archive_text',
                    'type' => 'textarea',
    
                ),
                array(
                    'name' => 'Wholesale title',
                    'id'   => 'p_wholesale_title',
                    'type' => 'text',
    
                ),
                 array(
                    'name' => 'Wholesale text',
                    'id'   => 'p_wholesale_text',
                    'type' => 'textarea',
    
                ),
                  array(
                    'name' => 'Wholesale phone',
                    'id'   => 'p_wholesale_phone',
                    'type' => 'text',
    
                ),
                   array(
                    'name' => 'Wholesale email',
                    'id'   => 'p_wholesale_email',
                    'type' => 'text',
    
                ),
            ),
        ),
         $meta_boxes[] = array(
            'id'             => 'stories',
            'title'          => 'Stories',
            'settings_pages' => 'settings',
            'tab'            => 'stories',

            'fields' => array(
                array(
                    'name' => 'Story archive text',
                    'id'   => 's_archive_text',
                    'type' => 'textarea',
    
                ),
            ),
        ),
        $meta_boxes[] = array(
            'id'             => 'socialmedia',
            'title'          => 'Social media',
            'settings_pages' => 'settings',
            'tab'            => 'social_media',
            'fields' => array(
                array(
                    'name' => 'Social link for Facebook',
                    'id'   => 'facebook',
                    'type' => 'text',
                    'placeholder' => 'Social link for Facebook',
                    'size' => 50
                ),
                array(
                    'name' => 'Social link for Draugiem',
                    'id'   => 'draugiem',
                    'type' => 'text',
                    'placeholder' => 'Social link for Draugiem',
                    'size' => 50
                ),
                array(
                    'name' => 'Social link for Youtube',
                    'id'   => 'youtube',
                    'type' => 'text',
                    'placeholder' => 'Social link for Youtube',
                    'size' => 50
                ),
                array(
                    'name' => 'Social link for Twitter',
                    'id'   => 'twitter',
                    'type' => 'text',
                    'placeholder' => 'Social link for Twitter',
                    'size' => 50
                ),
                array(
                    'name' => 'Social link for Google Plus',
                    'id'   => 'google-plus',
                    'type' => 'text',
                    'placeholder' => 'Social link for Google Plus',
                    'size' => 50
                ),
                array(
                    'name' => 'Social link for Instagram',
                    'id'   => 'instagram',
                    'type' => 'text',
                    'placeholder' => 'Social link for Instagram',
                    'size' => 50
                ),
            ),
        ),

        $meta_boxes[] = array(
            'id'             => 'customscripts',
            'title'          => 'Custom scripts',
            'settings_pages' => 'settings',
            'tab'            => 'custom_scripts',
            'fields' => array(
                array(
                    'name' => 'After head start tag',
                    'id'   => 'after_head',
                    'type' => 'textarea',
                    'placeholder' => 'After head start tag',
                    'rows' => 6,

                ),
                array(
                    'name' => 'Before Head end tag',
                    'id'   => 'before_head',
                    'type' => 'textarea',
                    'placeholder' => 'Before Head end tag',
                    'rows' => 6,

                ),
                array(
                    'name' => 'After Body start tag',
                    'id'   => 'after_body',
                    'type' => 'textarea',
                    'placeholder' => 'After Body start tag',
                    'rows' => 6,
                ),
                array(
                    'name' => 'Before Body end tag',
                    'id'   => 'before_body',
                    'type' => 'textarea',
                    'placeholder' => 'Before Body end tag',
                    'rows' => 6,
                ),

            ),
        ),

        $meta_boxes[] = array(
            'id'             => 'robotstxt',
            'title'          => 'Robots.txt',
            'settings_pages' => 'settings',
            'tab'            => 'robots',
            'fields' => array(
                array(
                    'name' => 'Robots.txt',
                    'id'   => 'robots',
                    'type' => 'textarea',
                    'label_description' => 'Example <br> User-Agent: * <br> Disallow: /folder/  <br> Disallow: /file.html',
                    'rows' => 10,
                ),
            ),
        ),

        $meta_boxes[] = array(
            'id'             => 'defsocial',
            'title'          => 'Default Social image',
            'settings_pages' => 'settings',
            'tab'            => 'def_soc_image',
            'fields' => array(
                array(
                    'name' => 'Default Social image',
                    'id'   => 'social_image',
                    'type' => 'image_advanced',
                    'label_description' => 'This image is used when there\'s no featured image set (for social portals) <br> Minimum size - 200x200px',
                    'max_file_uploads' => 1,
                    'max_status' => false,
                ),
            ),
        ),

        $meta_boxes[] = array(
            'id'             => 'redirects',
            'title'          => 'Redirects',
            'settings_pages' => 'settings',
            'tab'            => 'redirects',
            'fields' => array(
                array(
                    'name' => 'Redirects',
                    'id'   => 'redirects',
                    'type' => 'textarea',
                    'label_description' => 'This field is intended for redirected urls. The urls MUST be relative and contain appropriate language identificator. URLs must be formatted two in a row separated by semicolon (;). Example: /lv/aboli ; /lv/banani',
                    'rows' => 10,
                ),
            ),
        ),

        $meta_boxes[] = array(
            'id'             => 'gmap',
            'title'          => 'Google maps & analytics',
            'settings_pages' => 'settings',
            'tab'            => 'gmaps',
            'fields' => array(
                array(
                    'name' => 'Tracking ID',
                    'id'   => 'track_id',
                    'type' => 'text',
                    'size' => 50,
                ),
                array(
                    'name' => 'Google Maps javascript api key',
                    'id'   => 'map_api',
                    'type' => 'text',
                    'size' => 50,
                )
            ),
        )
    );
    return $meta_boxes;
}


