<?php

class Store{

    // plugin directory path
    public $store_url;

    public function __construct() {

        $this->store_url = get_template_directory_uri() . '/lib/stores/';


       $labels = array(
            'name'               => "Stores",
            'singular_name'      => "Store",
            'menu_name'          => "Stores",
            'add_new_item'       => "Add store",
            'new_item'           => "New store",
            'edit_item'          => "Edit store",
            'view_item'          => "View store",
            'all_items'          => "All stores",
            'search_items'       => "Search stores",
            'not_found'          => "No store found",
            'not_found_in_trash' => "No store found in trash"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'capabilities' => array(

            ),
            'map_meta_cap' => true,
            'hierarchical' => false,
            'has_archive' => false,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-admin-home',
            'supports' => array("title"),
        );

        register_post_type('store', $args);

        add_filter( 'rwmb_meta_boxes', 'caballero_smartmap_meta_boxes' );
        function caballero_smartmap_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'store',
                'title'     => 'Store information',
                'post_types' => array('store'),
                'context' => 'normal',
                'priority' => 'high',
                'fields' => array(
               
                            array(
                                'id'    => 'store_title',
                                'name' => 'Store title',
                                'type'  => 'text',
                                'size' => 60,
                            ),
                            array(
                                'id'    => 'city',
                                'name' => 'City',
                                'type'  => 'text',
                                'size' => 60,
                            ),
                            array(
                                'id'    => 'contact_person',
                                'name' => 'Contact person',
                                'type'  => 'text',
                                'size' => 60,
                            ),
                            array(
                                'id'    => 'phone',
                                'name' => 'Phone',
                                'type'  => 'text',
                                'size' => 60,
                            ),
                            array(
                                'id'    => 'hours',
                                'name' => 'Working hours',
                                'type'  => 'text',
                                'size' => 60,
                            ),
                            array(
                                'id'            => 'map',
                                'name'          => 'Location',
                                'type'          => 'map',
                                'address_field' => 'address',
                                'api_key'       => 'AIzaSyBaob1XUk6NQ7RmmsB4QuhqjGh3foYE3EE',
                            ),
                            array(
                                'id'   => 'address',
                                'name' => 'Adrese',
                                'type' => 'text',
                                'placeholder' => 'Adrese',
                                'size' => '80',
                            ),
                ),
            );

            return $meta_boxes;
        } /*End metabox*/

    }

}
new Store();