<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header-simple', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
$description = rwmb_meta( 'p_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
  <div class="archive-desc animate animate__fade">
    <p><?php echo e($description); ?></p>
  </div>
</div>

<div class="container posts">
  <div class="row">
    <?php while(have_posts()): ?> <?php the_post() ?>
      <div class="col-sm-4">
        <?php echo $__env->make('partials.content-'.get_post_type(), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
    <?php endwhile; ?>
  </div>
</div>
  

  <?php echo get_the_posts_navigation(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>