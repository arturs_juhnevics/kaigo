<?php $lang = pll_current_language('slug'); ?>
<footer class="footer">
  <div class="container animate animate__fade-up">
   <?php echo rwmb_meta( 'footer_text_'.$lang, array( 'object_type' => 'setting',  'limit' => 1 ), 'settings'); ?>
  </div>
</footer>
