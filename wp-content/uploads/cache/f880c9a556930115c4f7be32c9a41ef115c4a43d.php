<?php
$background_image = rwmb_meta('cta-text'); 
$heading = rwmb_meta('hero_heading'); 
$text = rwmb_meta('hero_text');
$btn = rwmb_meta('hero_button_name');
$btnUrl = rwmb_meta('hero_button_url');   
$images = rwmb_meta( 'hero_background', array( 'size' => 'large' ) );
$image = reset($images);
?>
<div class="hero" style="background-image: url(<?php echo $image['url']; ?>);">
	<div class="overlay">
	</div>
	<div class="container">
		<div class="hero__inner">
			<h1 class="animate animate__fade-up"><?php echo $heading; ?></h1>
			<p class="animate animate__fade-up"><?php echo $text; ?></p>
			<a class="button animate animate__fade-up" href="<?php echo $btnUrl; ?>"><?php echo $btn; ?></a>
		</div>
	</div>
</div>