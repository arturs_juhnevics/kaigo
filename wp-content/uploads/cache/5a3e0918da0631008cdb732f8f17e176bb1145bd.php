<div class="newsletter animate animate__fade-up delay4">
	<div class="newsletter__inner">
		<div class="newsletter__info">
			<h2 class="newsletter__title"><?php echo pll__('Subscribe to newsletter', 'Newsletter'); ?></h2>
			<p class="newsletter__text"><?php echo pll__('Newsletter text', 'Newsletter'); ?></p>
		</div>
		<div class="newsletter__status"></div>
		<form id="newsletter-form" class="newsletter__form"> 
      <div class="input__row">
        <input type="radio" name="sender-list" value="74081" id="sender-r1" class="form-radio" checked>
        <label class="input__block__label" for="sender-r1"><?php echo pll__('R1', 'Newsletter'); ?></label>
      </div>
      <div class="input__row">
        <input type="radio" name="sender-list" value="74079" id=sender-r3" class="form-radio">
        <label class="input__block__label" for="sender-r3"><?php echo pll__('R3', 'Newsletter'); ?></label>
      </div>
			<label class="input__block">
        	<span class="input__block__label"><?php echo pll__('Your email address', 'Contact-form'); ?><span class="req">*</span></span>
        	<input type="email" name="email" id="newsletter-email" class="required"/>
    	</label>
    	<div class="newsletter__buttons">
    		<button class="button" type="submit" class="newsletter__form__submit"><?php echo pll__('Subscribe', 'Newsletter'); ?></button>
    		<button class="button-close"><?php echo pll__('Not now', 'Newsletter'); ?></button>
    	</div>
    	 <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
        <?php $from = '';
        if (isset($_GET['from'])) {
            $from = strip_tags($_GET['from']);
        } ?>
        <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
		</form>
	</div>
</div>
