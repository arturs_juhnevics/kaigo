<?php
$work = get_posts( array(
    'post_type' => 'work',
    'numberposts' => -1,
    'post_status' => 'publish',
) );
?>
<div class="container">
	<div class="landing-work">
		<div class="landing-work__heading">
			<div class="row">
				<div class="col-sm-5">
					<h2 class="animate animate__fade-up">Work</h2>
				</div>
				<div class="col-sm-7">
					<div class="slider-navigation">
						<span class="arrow-left"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-left.svg"); ?></span>
						<span class="slider-dots"></span>
				         <span class="arrow-right">
				         	<?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-right.svg"); ?> 		
				        </span>
					</div>
				</div>
			</div>
			
			
		</div>
		<div class="work-slider">
			<?php $__currentLoopData = $work; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
					$image = get_the_post_thumbnail_url($item->ID);
					$title = get_the_title($item->ID);
					$text= rwmb_meta('short_description', false, $item->ID);  
					$materials= rwmb_meta('materials', false, $item->ID);  
					$url = get_the_permalink($item->ID);
				?>
				<div class="landing-work__item">
					<div class="row">
						<div class="col-sm-6">
							<h3 class="landing-work__item__title animate animate__fade-up"><?php echo e($title); ?></h3>
							<p class="landing-work__item__text animate animate__fade-up"><?php echo e($text); ?></p>
							<p class="landing-work__item__materials animate animate__fade-up">material
								<?php $__currentLoopData = $materials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $material): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<span class="animate animate__fade-up"><?php echo e($material); ?></span>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</p>
							<a href=<?php echo e($url); ?>"" class="button">View details</a>
						</div>
						<div class="col-sm-6">
							<div class="landing-work__item__image animate animate__fade-up" style="background-image: url(<?php echo e($image); ?>)">
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>