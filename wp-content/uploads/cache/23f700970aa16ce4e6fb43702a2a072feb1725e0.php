<?php
$heading = rwmb_meta('post_heading'); 
$heading_slug = rwmb_meta('post_slug'); 
$url = rwmb_meta('post_url'); 
$query = new WP_Query( array(
    'post_type' => 'post',
    'numberposts' => 3,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'     => 'feat_post',
            'value'   => '1',
            'compare' => 'LIKE',
        ),
    ),
) );
?>
<div class="container home-section">
	<div class="home-heading-content">
		<div class="home-heading-content__heading animate animate__fade">
			<p class="home-heading-content__slug"><?php echo e($heading_slug); ?></p>
			<h2 class="home-heading-content__title"><?php echo e($heading); ?></h2>
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more" href="<?php echo e($url); ?>">VIEW ALL STORIES</a>
		</div>
	</div>
	<div class="posts">
		<div class="row">
			<?php while($query->have_posts()): ?> <?php $query->the_post() ?>
				<div class="col-sm-4">
			  		<?php echo $__env->make('partials.content-post', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			  	</div>
		   <?php endwhile; ?>
		</div>
	</div>
	<div class="button-container mob-only">
		<a href="<?php echo e($url); ?>" class="button">VIEW ALL STORIES</a> 
	</div>
</div>