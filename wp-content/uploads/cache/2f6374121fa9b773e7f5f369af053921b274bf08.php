<?php 
$logos = rwmb_meta( 'page-header', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
?>
<?php if ( is_single() && 'post' == get_post_type() ) : ?>

	<?php
	$header_image = get_the_post_thumbnail_url();
	?>
	<div class="page-header page-header-notitle" style="background-image: url(<?php echo e($header_image); ?>);">
		<div class="overlay"></div>
		<?php //breadcrumbs(); ?>
	</div>
<?php else : ?>

	<div class="page-header" style="background-image: url(<?php echo e($header_image); ?>);">
		<div class="overlay"></div>
		<?php //breadcrumbs(); ?>
		<div class="page-header__title">
			<h1 class="animate animate__fade"><?php echo page_title() ?></h1>
		</div>
	</div>
<?php endif; ?>
